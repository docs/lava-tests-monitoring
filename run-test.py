#!/usr/bin/env python3

import re
import os
import sys
import subprocess


class LavaTestCase(object):
    """
    Main class implementing common methods and attributes for monitoring
    resources in LAVA.
    """

    def __init__(self):
        self.lava_stats_cmd = \
                "lava-test-case %s --result %s --measurement %f --units %s"

    def _parse(self):
        """Parse the test command output"""
        pass

    def run(self):
        """Execute the test program"""
        pass

class TimeCmd(LavaTestCase):

    def __init__(self, test_cmd, maximum_rss=None):
        super().__init__()
        self._cmd = '/usr/bin/time'
        self._format = "cpu_percentage:%P:percentage\n" \
                       "maximum_resident_set_size:%M:kbs\n" \
                       "elapsed_real_time:%e:secs"
        self._maximum_rss = maximum_rss
        self.test_cmd = test_cmd

    def _parse(self):
        for line in self.proc.stderr.split():
            if re.match('^\w+:[\d\.%]+:\w+$', line):
                name, measurement, units = line.split(':')
                result = 'pass'

                # Check for a maximum value in the 'maximum_resident_set_size'
                # and 'fail' the metric test if it goes beyond that.
                if name == 'maximum_resident_set_size':
                    if self._maximum_rss and \
                       float(measurement) > self._maximum_rss:
                        result = 'fail'

                # Remove the '%' from the cpu_percentage value
                m = measurement[:-1] if measurement[-1] == '%' else measurement

                # Use plain 'system' call for the lava-test-case command
                os.system(self.lava_stats_cmd % (name, result, float(m), units))

    def run(self):
        self.proc = subprocess.run([self._cmd, '-f', self._format] + self.test_cmd,
                                   stderr=subprocess.PIPE, encoding='utf-8')
        self._parse()


# Run the test program.
if len(sys.argv) > 1:
    # Test the "ps aux" command with a metric value limit.
    # Fail the test if the metric value goes beyond the limit.
    test_prog = TimeCmd(["ps", "aux"], maximum_rss=1000)
    test_prog.run()
else:
    # Test the "ls -lt" command without any metric value limit.
    test_prog = TimeCmd(["ls", "-lt"])
    test_prog.run()
